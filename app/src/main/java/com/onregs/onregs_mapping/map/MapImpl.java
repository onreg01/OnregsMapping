package com.onregs.onregs_mapping.map;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.onregs.onregs_mapping.data.Photo;
import com.onregs.onregs_mapping.data.Place;
import com.onregs.onregs_mapping.utils.Launcher;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vadim on 23.10.2015.
 */
public class MapImpl implements Map, GoogleMap.OnMarkerClickListener
{

    public Context context;

    public MapImpl(Context context)
    {
        this.context = context;
    }

    private HashMap<String, ArrayList<Photo>> markersId;

    @Override
    public void initMap(GoogleMap map, ArrayList<ArrayList<Photo>> photos)
    {
        markersId = new HashMap<String, ArrayList<Photo>>();

        for (ArrayList<Photo> listPhotos : photos)
        {
            addMarkerToPlace(map, listPhotos);
        }
    }

    private void addMarkerToPlace(GoogleMap map, ArrayList<Photo> listPhotos)
    {
        Place place = listPhotos.get(0).getPlace();

        Marker marker = map.addMarker(new MarkerOptions().position(new LatLng(place.getLatitude(), place.getLongitude())));
        markersId.put(marker.getId(), listPhotos);
        map.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(Marker marker)
    {
        Launcher.startPhotosActivity(context, markersId.get(marker.getId()));
        return false;
    }
}
