package com.onregs.onregs_mapping.map;

import com.google.android.gms.maps.GoogleMap;
import com.onregs.onregs_mapping.data.Photo;

import java.util.ArrayList;

/**
 * Created by vadim on 23.10.2015.
 */
public interface Map
{
    void initMap(GoogleMap map, ArrayList<ArrayList<Photo>> photos);
}
