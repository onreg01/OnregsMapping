package com.onregs.onregs_mapping.data.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.graphics.Bitmap;

import com.onregs.onregs_mapping.facebooksdk.FaceBookActions_;

/**
 * Created by vadim on 21.10.2015.
 */
public class SharePictureLoader extends AsyncTaskLoader<String>
{
    private Bitmap image;
    private String message;

    public SharePictureLoader(Context context, String message, Bitmap image)
    {
        super(context);
        this.image = image;
        this.message = message;
    }

    public SharePictureLoader(Context context)
    {
        super(context);
    }

    @Override
    public String loadInBackground()
    {
        return FaceBookActions_.getInstance_(getContext()).sharePicture(image, message);
    }
}
