package com.onregs.onregs_mapping.data;

import java.io.Serializable;

/**
 * Created by vadim on 23.10.2015.
 */
public class Image implements Serializable
{
    private String source;
    private int height;
    private int width;
    private int size;

    public Image(String source, int height, int width)
    {
        this.source = source;
        this.height = height;
        this.width = width;
        size = height*width;
    }

    public String getSource()
    {
        return source;
    }

    public int getHeight()
    {
        return height;
    }

    public int getWidth()
    {
        return width;
    }

    public int getSize()
    {
        return size;
    }
}
