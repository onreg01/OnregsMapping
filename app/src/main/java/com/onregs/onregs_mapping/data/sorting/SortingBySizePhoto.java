package com.onregs.onregs_mapping.data.sorting;

import com.onregs.onregs_mapping.data.Image;

import java.util.Comparator;

/**
 * Created by vadim on 24.10.2015.
 */
public class SortingBySizePhoto implements Comparator<Image>
{
    @Override
    public int compare(Image imageFirst, Image imageSecond)
    {
        int firstSize = imageFirst.getSize();
        int secondSize = imageSecond.getSize();

        if (firstSize < secondSize)
        {
            return 1;
        }
        else if (firstSize > secondSize)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }
}
