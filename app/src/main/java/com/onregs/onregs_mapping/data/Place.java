package com.onregs.onregs_mapping.data;

import java.io.Serializable;

/**
 * Created by vadim on 23.10.2015.
 */
public class Place implements Serializable
{
    private String placeId;
    private double longitude;
    private double latitude;

    public Place(String placeId, double longitude, double latitude)
    {
        this.placeId = placeId;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getPlaceId()
    {
        return placeId;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public double getLatitude()
    {
        return latitude;
    }

}
