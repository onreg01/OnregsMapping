package com.onregs.onregs_mapping.data.loader;

import android.content.Context;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.onregs.onregs_mapping.R;
import com.onregs.onregs_mapping.data.Photo;

/**
 * Created by vadim on 25.10.2015.
 */
public class ImagesLoader
{
    private com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    private DisplayImageOptions options;

    public static final int IMAGE_600x450 = 270000;

    public ImagesLoader(Context context)
    {
        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
        imageLoader.init(config);
        setOptions();
    }

    private void setOptions()
    {
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_stub)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
    }

    public void loadImage(Photo photo, ImageView imageView, int size)
    {
        imageLoader.displayImage(photo.getImageWithSize(size).getSource(), imageView, options);
    }


}
