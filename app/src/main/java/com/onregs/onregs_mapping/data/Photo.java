package com.onregs.onregs_mapping.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vadim on 23.10.2015.
 */
public class Photo implements Serializable
{
    private String message;
    private String id;
    private Place place;
    private ArrayList<Image> images;


    public Photo(String name, String id)
    {
        this.message = name;
        this.id = id;
    }

    public String getMessage()
    {
        return message;
    }

    public String getId()
    {
        return id;
    }

    public Place getPlace()
    {
        return place;
    }

    public void setPlace(Place place)
    {
        this.place = place;
    }

    public ArrayList<Image> getImages()
    {
        return images;
    }

    public void setImages(ArrayList<Image> images)
    {
        this.images = images;
    }

    public Image getImageWithSize(int size)
    {
        Image photo = null;
        for (Image image : images)
        {
            if (image.getSize() <= size)
            {
                photo = image;
                break;
            }
        }
        return photo;
    }

    @Override
    public boolean equals(Object object)
    {
        boolean isEqual = false;

        if (object != null && object instanceof Photo)
        {
            isEqual = (this.place.getPlaceId().equals(((Photo) object).getPlace().getPlaceId()));
        }

        return isEqual;
    }

}
