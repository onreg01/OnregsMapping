package com.onregs.onregs_mapping.utils;

import com.facebook.GraphResponse;
import com.onregs.onregs_mapping.data.Image;
import com.onregs.onregs_mapping.data.Photo;
import com.onregs.onregs_mapping.data.Place;
import com.onregs.onregs_mapping.data.sorting.SortingBySizePhoto;
import com.onregs.onregs_mapping.facebooksdk.FaceBookSdkConst;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by vadim on 23.10.2015.
 */
public class JSONUtils
{
    private GraphResponse graphResponse;

    public JSONUtils(GraphResponse graphResponse)
    {
        this.graphResponse = graphResponse;
    }


    public String getPlaceId()
    {

        String placeId = FaceBookSdkConst.ERROR;

        try
        {
            JSONObject jsonObject = new JSONObject(graphResponse.getRawResponse());
            if (jsonObject.getJSONArray(FaceBookSdkConst.DATA).length() != 0)
            {
                placeId = jsonObject.getJSONArray(FaceBookSdkConst.DATA).getJSONObject(0).getString(FaceBookSdkConst.ID);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return placeId;
    }

    public ArrayList<ArrayList<Photo>> getListPhotos()
    {
        ArrayList<Photo> photos = new ArrayList<Photo>();
        try
        {
            JSONArray data = graphResponse.getJSONObject().getJSONArray(FaceBookSdkConst.DATA);
            for (int i = 0; i < data.length(); i++)
            {
                JSONObject jsonObject = data.getJSONObject(i);

                if (!jsonObject.has(FaceBookSdkConst.PLACE))
                {
                    continue;
                }

                Photo photo = new Photo(getMessage(jsonObject), jsonObject.getString(FaceBookSdkConst.ID));
                photo.setPlace(getPlace(jsonObject));
                photo.setImages(getImages(jsonObject));
                photos.add(photo);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return getPhotosWithSimilarPlaces(photos);
    }

    private Place getPlace(JSONObject jsonObject) throws JSONException
    {
        JSONObject jsonObjectPlace = jsonObject.getJSONObject(FaceBookSdkConst.PLACE);
        JSONObject jsonObjectLocation = jsonObjectPlace.getJSONObject(FaceBookSdkConst.LOCATION);

        return new Place(jsonObjectPlace.getString(FaceBookSdkConst.ID),
                jsonObjectLocation.getDouble(FaceBookSdkConst.LONGITUDE), jsonObjectLocation.getDouble(FaceBookSdkConst.LATITUDE));
    }

    private ArrayList<Image> getImages(JSONObject jsonObject) throws JSONException
    {
        ArrayList<Image> images = new ArrayList<Image>();
        JSONArray jsonArrayImages = jsonObject.getJSONArray(FaceBookSdkConst.IMAGES);
        for (int i = 0; i < jsonArrayImages.length(); i++)
        {
            JSONObject jsonImage = jsonArrayImages.getJSONObject(i);
            Image image = new Image(jsonImage.getString(FaceBookSdkConst.SOURCE),
                    jsonImage.getInt(FaceBookSdkConst.HEIGHT), jsonImage.getInt(FaceBookSdkConst.WIDTH));
            images.add(image);
        }
        Collections.sort(images, new SortingBySizePhoto());
        return images;
    }

    private String getMessage(JSONObject jsonObject) throws JSONException
    {
        String message = "";

        if (jsonObject.has(FaceBookSdkConst.NAME))
        {
            message = jsonObject.getString(FaceBookSdkConst.NAME);
        }
        return message;
    }


    private ArrayList<ArrayList<Photo>> getPhotosWithSimilarPlaces(ArrayList<Photo> photos)
    {
        ArrayList<ArrayList<Photo>> similarPlacesPhotos = new ArrayList<ArrayList<Photo>>();

        while (!photos.isEmpty())
        {
            ArrayList<Photo> groupPhotos = new ArrayList<Photo>();
            groupPhotos.add(photos.get(0));
            photos.remove(0);

            for (int i = 0; i < photos.size(); i++)
            {
                if (groupPhotos.contains(photos.get(i)))
                {
                    groupPhotos.add(photos.get(i));
                    photos.remove(i);
                    i--;
                }
            }
            similarPlacesPhotos.add(groupPhotos);
        }
        return similarPlacesPhotos;
    }
}
