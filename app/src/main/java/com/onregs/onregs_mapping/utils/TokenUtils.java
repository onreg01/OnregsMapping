package com.onregs.onregs_mapping.utils;

import android.content.Context;

import com.facebook.AccessToken;
import com.facebook.AccessTokenSource;
import com.onregs.onregs_mapping.settings.SettingsFacade_;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;

/**
 * Created by vadim on 21.10.2015.
 */
public class TokenUtils
{
    public static AccessToken createAppAccessToken(String token, Context context)
    {

        AccessToken userToken = AccessToken.getCurrentAccessToken();

        String appId = userToken.getApplicationId();
        String userId = userToken.getUserId();
        Set<String> permissions = userToken.getPermissions();
        Set<String> declinePermissions = userToken.getDeclinedPermissions();
        AccessTokenSource accessTokenSource = userToken.getSource();

        Calendar calendar = Calendar.getInstance();
        Date lastRefresh = calendar.getTime();

        calendar.add(Calendar.MONTH, 2);
        calendar.getTime();
        Date expires = calendar.getTime();

        AccessToken newAccessToken =
                new AccessToken(token, appId, userId, permissions, declinePermissions, accessTokenSource, expires, lastRefresh);

        SettingsFacade_.getInstance_(context).setAppToken(GsonUtils.convertToken(newAccessToken));
        return newAccessToken;
    }
}
