package com.onregs.onregs_mapping.utils;

import com.facebook.AccessToken;
import com.google.gson.Gson;

/**
 * Created by vadim on 19.10.2015.
 */
public class GsonUtils
{

    public static String convertToken(AccessToken accessToken)
    {
        return new Gson().toJson(accessToken);
    }

    public static AccessToken restoreToken(String accessToken)
    {
        return new Gson().fromJson(accessToken, AccessToken.class);
    }
}
