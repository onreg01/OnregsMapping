package com.onregs.onregs_mapping.utils;

import android.content.Context;

import im.delight.android.location.SimpleLocation;

/**
 * Created by vadim on 21.10.2015.
 */
public class LocationUtils
{
    public static String getStrLocation(Context context)
    {
        SimpleLocation location = new SimpleLocation(context);
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        return String.valueOf(latitude)+","+String.valueOf(longitude);
    }
}
