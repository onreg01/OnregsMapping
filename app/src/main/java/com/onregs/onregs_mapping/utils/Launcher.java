package com.onregs.onregs_mapping.utils;

import android.content.Context;

import com.onregs.onregs_mapping.data.Photo;
import com.onregs.onregs_mapping.ui.activity.PhotosActivity_;

import java.util.ArrayList;

/**
 * Created by vadim on 24.10.2015.
 */
public class Launcher
{
    public static void startPhotosActivity(Context context, ArrayList<Photo> photos)
    {
        PhotosActivity_.intent(context).photos(photos).start();
    }
}
