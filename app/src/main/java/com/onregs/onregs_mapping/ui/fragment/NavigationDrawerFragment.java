package com.onregs.onregs_mapping.ui.fragment;


import android.app.Activity;
import android.app.Fragment;

import com.onregs.onregs_mapping.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.fragment_navigation_drawer)
public class NavigationDrawerFragment extends Fragment
{
    private NavigationListener navigationListener;
    private NavigationDrawerCloser drawerCloser;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (activity instanceof NavigationDrawerCloser)
        {
            drawerCloser = (NavigationDrawerCloser) activity;
        }
        if (activity instanceof NavigationListener)
        {
            navigationListener = (NavigationListener) activity;
        }
    }

    @Click
    public void tvShareMessage()
    {
        performNavigation(NavigationItem.NAVIGATION_SHARE_STATUS);
        closeDrawer();
    }

    @Click
    public void tvSharePicture()
    {
        performNavigation(NavigationItem.NAVIGATION_SHARE_PICTURE);
        closeDrawer();
    }

    @Click
    public void tvLogin()
    {
        performNavigation(NavigationItem.NAVIGATION_LOGIN);
        closeDrawer();
    }

    @Click
    public void tvLogOut()
    {
        performNavigation(NavigationItem.NAVIGATION_LOG_OUT);
        closeDrawer();
    }


    private void performNavigation(NavigationItem item)
    {
        if (navigationListener != null)
        {
            navigationListener.onPerformNavigation(item);
        }
    }

    private void closeDrawer()
    {
        if (drawerCloser != null)
        {
            drawerCloser.closeDrawer();
        }
    }
}
