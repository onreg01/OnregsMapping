package com.onregs.onregs_mapping.ui.view.edit_text;

import android.text.TextWatcher;

/**
 * Created by vadim on 20.12.2014.
 */
public abstract class CustomTextWatcher implements TextWatcher
{
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3)
    {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3)
    {

    }
}
