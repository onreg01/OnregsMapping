package com.onregs.onregs_mapping.ui.activity;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.onregs.onregs_mapping.R;
import com.onregs.onregs_mapping.data.Photo;
import com.onregs.onregs_mapping.data.loader.SharePictureLoader;
import com.onregs.onregs_mapping.map.Map;
import com.onregs.onregs_mapping.map.MapImpl;
import com.onregs.onregs_mapping.settings.SettingsFacade_;
import com.onregs.onregs_mapping.ui.fragment.NavigationDrawerCloser;
import com.onregs.onregs_mapping.ui.fragment.NavigationItem;
import com.onregs.onregs_mapping.ui.fragment.NavigationListener;
import com.onregs.onregs_mapping.ui.fragment.SharePictureFragment;
import com.onregs.onregs_mapping.ui.fragment.dialog.ShareMessageDialog;
import com.onregs.onregs_mapping.ui.presenter.StartActivityPresenter;
import com.onregs.onregs_mapping.ui.presenter.StartActivityPresenterImpl;
import com.onregs.onregs_mapping.ui.view.StartActivityView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_start)
@OptionsMenu(R.menu.menu_activity_start)
public class StartActivity extends Activity implements
        StartActivityView, NavigationDrawerCloser, NavigationListener, LoaderManager.LoaderCallbacks<String>,
        ShareMessageDialog.ShareMessageDialogInterface, SharePictureFragment.SharePictureFragmentInterface
{
    @Bean(StartActivityPresenterImpl.class)
    StartActivityPresenter presenter;

    @ViewById(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @ViewById(R.id.left_drawer)
    View drawerContainer;

    @ViewById
    TextView tvStatus;

    @FragmentById
    MapFragment mapFragment;

    private ActionBarDrawerToggle drawerToggle;
    private GoogleMap googleMap;

    public static CallbackManager callbackmanager;
    public static final String MESSAGE = "message";
    public static final String IMAGE = "image";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackmanager = CallbackManager.Factory.create();
        getLoaderManager().initLoader(1, null, this);
    }

    @AfterViews
    public void init()
    {
        presenter.init(this);
        initDrawerToggle();
        initMap();
        restoreUserData();
    }

    private void restoreUserData()
    {
        if (checkToken())
        {
            setStatusUser(SettingsFacade_.getInstance_(this).getUser());
            presenter.refresh();
        }
        else
        {
            setStatusNoConnect();
        }
    }

    private void initMap()
    {
        googleMap = mapFragment.getMap();
    }

    private void initDrawerToggle()
    {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, new Toolbar(this), R.string.drawer_open, R.string.drawer_close);
        drawerLayout.setDrawerListener(drawerToggle);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }


    @OptionsItem(R.id.menu_refresh)
    void menuRefreshSelected()
    {
        if (!checkToken())
        {
            showMessage(getResources().getString(R.string.no_connect_to_facebook));
            return;
        }
        presenter.refresh();
    }

    @Override
    public Activity getContainer()
    {
        return this;
    }

    @Override
    public void closeDrawer()
    {
        drawerLayout.closeDrawer(drawerContainer);
    }

    @Override
    public void onPerformNavigation(NavigationItem item)
    {
        if (!checkNetworkStatus())
        {
            showMessage(getResources().getString(R.string.network_error));
            return;
        }
        switch (item)
        {
            case NAVIGATION_SHARE_STATUS:
                shareStatus();
                break;
            case NAVIGATION_SHARE_PICTURE:
                sharePicture();
                break;
            case NAVIGATION_LOGIN:
                presenter.login(this, callbackmanager);
                break;
            case NAVIGATION_LOG_OUT:
                presenter.logOut();
                break;
            default:
                break;
        }
    }

    private void shareStatus()
    {
        if (!checkToken())
        {
            showMessage(getResources().getString(R.string.no_connect_to_facebook));
            return;
        }
        DialogFragment shareMessageDialog = ShareMessageDialog.build();
        shareMessageDialog.show(getFragmentManager(), ShareMessageDialog.TAG);
    }

    private void sharePicture()
    {
        if (!checkToken())
        {
            showMessage(getResources().getString(R.string.no_connect_to_facebook));
            return;
        }
        getFragmentManager().beginTransaction()
                .replace(R.id.container, SharePictureFragment.build(), SharePictureFragment.TAG)
                .addToBackStack(SharePictureFragment.TAG)
                .commit();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        callbackmanager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    public void updateStatusUser(String user)
    {
        setStatusUser(user);
    }

    @Override
    public void logOut()
    {
        setStatusNoConnect();
    }

    @Override
    public void showMessage(String statusUpdated)
    {
        Toast.makeText(this, statusUpdated, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if (checkToken())
        {
            SettingsFacade_.getInstance_(this).setUser(tvStatus.getText().toString());
        }
    }

    private boolean checkToken()
    {
        return (AccessToken.getCurrentAccessToken() != null);

    }

    private boolean checkNetworkStatus()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null;
    }

    private void setStatusUser(String user)
    {
        tvStatus.setText(user);
    }

    private void setStatusNoConnect()
    {
        tvStatus.setText(getResources().getString(R.string.no_connection));
    }

    @Override
    public Loader<String> onCreateLoader(int i, Bundle bundle)
    {
        Loader<String> loader;
        if (bundle == null)
        {
            loader = new SharePictureLoader(this);
        }
        else
        {
            loader = new SharePictureLoader(this, bundle.getString(MESSAGE), (Bitmap) bundle.getParcelable(IMAGE));
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<String> stringLoader, String message)
    {
        if (message.equals(getResources().getString(R.string.uploaded_image)))
        {
            presenter.refresh();
        }
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoaderReset(Loader<String> stringLoader)
    {

    }

    @Override
    public void updateStatus(String message)
    {
        presenter.updateStatus(message);
    }

    @Override
    public void startDownload(Bitmap image, String message)
    {
        Bundle bundle = new Bundle();
        bundle.putParcelable(IMAGE, image);
        bundle.putString(MESSAGE, message);
        getLoaderManager().restartLoader(1, bundle, this).forceLoad();
    }

    @Override
    public void initMap(ArrayList<ArrayList<Photo>> photos)
    {
        googleMap.clear();
        Map map = new MapImpl(this);
        map.initMap(googleMap, photos);
    }
}
