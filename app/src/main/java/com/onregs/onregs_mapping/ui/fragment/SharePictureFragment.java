package com.onregs.onregs_mapping.ui.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.onregs.onregs_mapping.R;
import com.onregs.onregs_mapping.ui.activity.StartActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

/**
 * Created by vadim on 21.10.2015.
 */
@EFragment(R.layout.fragment_share_picture)
public class SharePictureFragment extends Fragment
{
    public static final String TAG = SharePictureFragment.class.getSimpleName();

    public static SharePictureFragment build()
    {
        return SharePictureFragment_.builder().build();
    }

    public static final int RESULT_LOAD_IMAGE = 1;

    @ViewById
    ImageView ivSharePicture;

    @ViewById
    EditText etMessage;

    @InstanceState
    boolean isImageEmpty = true;

    @InstanceState
    Bitmap currentImage;

    SharePictureFragmentInterface sharePictureFragmentInterface;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (activity instanceof StartActivity)
        {
            this.sharePictureFragmentInterface = (SharePictureFragmentInterface) activity;
        }
    }

    @AfterViews
    public void init()
    {
        if (currentImage != null)
        {
            etMessage.requestFocus();
            ivSharePicture.setImageBitmap(currentImage);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Click(R.id.btnSharePhoto)
    public void sendPicture()
    {
        final Editable message = etMessage.getText();
        if (!(TextUtils.isEmpty(message)) && (!isImageEmpty))
        {
            sharePictureFragmentInterface.startDownload(currentImage, message.toString());
            Toast.makeText(getActivity(), getResources().getString(R.string.start_loading_image), Toast.LENGTH_SHORT).show();
            getFragmentManager().popBackStack();
        }
    }

    @Click(R.id.ivSharePicture)
    public void sharePicture()
    {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    private String getImagePathFromCursor(Intent data)
    {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();

        return picturePath;
    }

    @OnActivityResult(RESULT_LOAD_IMAGE)
    void onResult(int resultCode, Intent data)
    {
        if (resultCode == getActivity().RESULT_OK)
        {
            currentImage = BitmapFactory.decodeFile(getImagePathFromCursor(data));
            ivSharePicture.setImageBitmap(currentImage);
            isImageEmpty = false;
        }
    }

    public interface SharePictureFragmentInterface
    {
        void startDownload(Bitmap image, String message);
    }
}
