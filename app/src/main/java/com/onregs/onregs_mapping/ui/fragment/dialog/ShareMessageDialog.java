package com.onregs.onregs_mapping.ui.fragment.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.onregs.onregs_mapping.R;
import com.onregs.onregs_mapping.ui.activity.StartActivity;
import com.onregs.onregs_mapping.ui.view.edit_text.CustomTextWatcher;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;

/**
 * Created by vadim on 21.10.2015.
 */
@EFragment
public class ShareMessageDialog extends DialogFragment
{
    public static final String TAG = ShareMessageDialog.class.getSimpleName();

    public static ShareMessageDialog build()
    {
        return ShareMessageDialog_.builder().build();
    }

    private Button buttonSave;
    private EditText editText;

    @InstanceState
    boolean enabledButton;

    private ShareMessageDialogInterface shareMessageDialogInterface;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (activity instanceof StartActivity)
        {
            shareMessageDialogInterface = (ShareMessageDialogInterface) activity;
        }
    }

    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_DARK);
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_field, null);
        editText = (EditText) view.findViewById(R.id.tvEditText);
        editText.addTextChangedListener(textWatcher);

        builder.setTitle(getResources().getString(R.string.share_status))
                .setView(view)
                .setPositiveButton(getResources().getString(R.string.accept), positiveButton)
                .setNegativeButton(getResources().getString(R.string.cancel), null);

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(showListener);
        return dialog;
    }

    CustomTextWatcher textWatcher = new CustomTextWatcher()
    {
        @Override
        public void afterTextChanged(Editable editable)
        {
            if (TextUtils.isEmpty(editable))
            {
                enabledButton = false;
                switchButtonState(false, getResources().getColor(R.color.back));
            }
            else
            {
                enabledButton = true;
                switchButtonState(true, getResources().getColor(R.color.white));
            }
        }
    };


    DialogInterface.OnClickListener positiveButton = new DialogInterface.OnClickListener()
    {
        @Override
        public void onClick(DialogInterface dialogInterface, int i)
        {
            shareMessageDialogInterface.updateStatus(editText.getText().toString());
        }
    };

    DialogInterface.OnShowListener showListener = new DialogInterface.OnShowListener()
    {
        @Override
        public void onShow(DialogInterface dialog)
        {
            buttonSave = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
            if (enabledButton)
            {
                switchButtonState(true, getResources().getColor(R.color.white));
            }
            else
            {
                switchButtonState(false, getResources().getColor(R.color.back));
            }
        }
    };

    private void switchButtonState(Boolean stateClickable, int color)
    {
        if (buttonSave != null)
        {
            buttonSave.setClickable(stateClickable);
            buttonSave.setTextColor(color);
        }
    }

    public interface ShareMessageDialogInterface
    {
        void updateStatus(String message);
    }
}
