package com.onregs.onregs_mapping.ui.presenter;

import com.facebook.CallbackManager;
import com.onregs.onregs_mapping.ui.activity.StartActivity;
import com.onregs.onregs_mapping.ui.view.StartActivityView;

/**
 * Created by vadim on 18.10.2015.
 */
public interface StartActivityPresenter extends BasePresenter<StartActivityView>
{
    void login(StartActivity activity, CallbackManager callbackmanager);

    void logOut();

    void updateStatus(String status);

    void refresh();
}
