package com.onregs.onregs_mapping.ui.fragment;

/**
 * Created by vadim on 18.10.2015.
 */
public enum NavigationItem
{
    NAVIGATION_SHARE_STATUS(),
    NAVIGATION_SHARE_PICTURE(),
    NAVIGATION_LOGIN(),
    NAVIGATION_LOG_OUT();
}
