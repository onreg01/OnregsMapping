package com.onregs.onregs_mapping.ui.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.onregs.onregs_mapping.data.Photo;
import com.onregs.onregs_mapping.ui.adapters.binder.PhotosViewPagerItem;
import com.onregs.onregs_mapping.ui.adapters.binder.PhotosViewPagerItem_;

import java.util.ArrayList;

/**
 * Created by vadim on 24.10.2015.
 */
public class PhotosViewPagerAdapter extends PagerAdapter
{
    private ArrayList<Photo> photos;
    private Context context;


    public PhotosViewPagerAdapter(Context context, ArrayList<Photo> photos)
    {
        this.context = context;
        this.photos = photos;
    }

    @Override
    public int getCount()
    {
        return photos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object)
    {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {
        container.removeView((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
        View view = PhotosViewPagerItem_.build(context);
        ((PhotosViewPagerItem) view).bind(context, photos.get(position));
        container.addView(view);
        return view;
    }

}
