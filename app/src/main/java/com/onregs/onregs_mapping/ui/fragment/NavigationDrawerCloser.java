package com.onregs.onregs_mapping.ui.fragment;

/**
 * Created by vadim on 18.10.2015.
 */
public interface NavigationDrawerCloser
{
    void closeDrawer();
}
