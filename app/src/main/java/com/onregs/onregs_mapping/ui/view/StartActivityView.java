package com.onregs.onregs_mapping.ui.view;

import com.onregs.onregs_mapping.data.Photo;

import java.util.ArrayList;

/**
 * Created by vadim on 18.10.2015.
 */
public interface StartActivityView extends BaseView
{
    void updateStatusUser(String user);
    void logOut();
    void showMessage(String result);
    void initMap(ArrayList<ArrayList<Photo>> photos);
}
