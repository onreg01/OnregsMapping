package com.onregs.onregs_mapping.ui.activity;

import android.app.Activity;
import android.support.v4.view.ViewPager;

import com.onregs.onregs_mapping.R;
import com.onregs.onregs_mapping.data.Photo;
import com.onregs.onregs_mapping.ui.adapters.PhotosViewPagerAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;


@EActivity(R.layout.activity_photos)
public class PhotosActivity extends Activity
{
    @Extra
    ArrayList<Photo> photos;

    @ViewById(R.id.pager)
    ViewPager viewPager;


    @AfterViews
    public void init()
    {
        PhotosViewPagerAdapter adapter = new PhotosViewPagerAdapter(this, photos);
        viewPager.setAdapter(adapter);
    }
}
