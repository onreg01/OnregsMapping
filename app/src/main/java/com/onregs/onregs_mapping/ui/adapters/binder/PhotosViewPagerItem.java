package com.onregs.onregs_mapping.ui.adapters.binder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onregs.onregs_mapping.R;
import com.onregs.onregs_mapping.data.Photo;
import com.onregs.onregs_mapping.data.loader.ImagesLoader;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by vadim on 24.10.2015.
 */
@EViewGroup(R.layout.photos_view_pager_item)
public class PhotosViewPagerItem extends LinearLayout
{
    public PhotosViewPagerItem(Context context)
    {
        super(context);
    }

    @ViewById
    TextView tvMessage;

    @ViewById
    ImageView ivPhoto;

    public View bind(Context context, Photo photo)
    {
        setImage(photo, context);
        tvMessage.setText(photo.getMessage());
        return this;
    }

    private void setImage(Photo photo, Context context)
    {
        new ImagesLoader(context).loadImage(photo, ivPhoto, ImagesLoader.IMAGE_600x450);
    }
}
