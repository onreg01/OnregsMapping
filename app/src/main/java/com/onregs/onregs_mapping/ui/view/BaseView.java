package com.onregs.onregs_mapping.ui.view;

import android.app.Activity;

/**
 * Created by vadim on 18.10.2015.
 */
public interface BaseView
{
    Activity getContainer();
}
