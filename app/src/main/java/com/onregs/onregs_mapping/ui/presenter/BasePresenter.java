package com.onregs.onregs_mapping.ui.presenter;

import com.onregs.onregs_mapping.ui.view.BaseView;

/**
 * Created by vadim on 18.10.2015.
 */
public interface BasePresenter<T extends BaseView>
{
    void init(T view);
}
