package com.onregs.onregs_mapping.ui.presenter;

import com.facebook.CallbackManager;
import com.onregs.onregs_mapping.data.Photo;
import com.onregs.onregs_mapping.facebooksdk.FaceBookActions;
import com.onregs.onregs_mapping.facebooksdk.FaceBookActionsListener;
import com.onregs.onregs_mapping.ui.activity.StartActivity;
import com.onregs.onregs_mapping.ui.view.StartActivityView;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;

/**
 * Created by vadim on 18.10.2015.
 */
@EBean
public class StartActivityPresenterImpl implements StartActivityPresenter, FaceBookActionsListener
{

    @Bean
    FaceBookActions faceBookActions;


    private StartActivityView view;

    @Override
    public void init(StartActivityView view)
    {
        this.view = view;
    }

    @Override
    public void login(StartActivity activity, CallbackManager callbackmanager)
    {
        faceBookActions.login(activity, callbackmanager, this);
    }

    @Override
    public void logOut()
    {
        faceBookActions.logOut(this);
    }

    @Override
    public void refresh()
    {
        faceBookActions.getImageData(this);
    }


    @Override
    public void updateStatus(String status)
    {
        faceBookActions.updateStatus(status, this);
    }

    @Override
    public void loginActions(String user)
    {
        view.updateStatusUser(user);
    }

    @Override
    public void logOutActions()
    {
        view.logOut();
    }

    @Override
    public void updateStatusActions(String statusUpdated)
    {
        view.showMessage(statusUpdated);
    }

    @Override
    public void refreshActions(ArrayList<ArrayList<Photo>> photos)
    {
        view.initMap(photos);
    }
}
