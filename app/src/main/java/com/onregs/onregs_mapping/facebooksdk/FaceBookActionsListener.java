package com.onregs.onregs_mapping.facebooksdk;


import com.onregs.onregs_mapping.data.Photo;

import java.util.ArrayList;

/**
 * Created by vadim on 19.10.2015.
 */
public interface FaceBookActionsListener
{
    void loginActions(String user);

    void logOutActions();

    void updateStatusActions(String statusUpdate);

    void refreshActions(ArrayList<ArrayList<Photo>> photos);
}
