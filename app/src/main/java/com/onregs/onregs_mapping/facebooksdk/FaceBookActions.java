package com.onregs.onregs_mapping.facebooksdk;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.onregs.onregs_mapping.R;
import com.onregs.onregs_mapping.settings.SettingsFacade_;
import com.onregs.onregs_mapping.utils.GsonUtils;
import com.onregs.onregs_mapping.utils.JSONUtils;
import com.onregs.onregs_mapping.utils.LocationUtils;
import com.onregs.onregs_mapping.utils.TokenUtils;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vadim on 19.10.2015.
 */
@EBean(scope = EBean.Scope.Singleton)
public class FaceBookActions
{

    private static final int MIN_DISTANCE = 100;

    @RootContext
    Context rootContext;

    private int minDistance = MIN_DISTANCE;
    private AccessToken appAccessToken;


    public void login(Activity activity, CallbackManager callbackmanager, final FaceBookActionsListener faceBookActionsListener)
    {
        LoginManager.getInstance().logInWithReadPermissions(activity, FaceBookSdkConst.PERMISSIONS);

        LoginManager.getInstance().registerCallback(callbackmanager, new FacebookCallback<LoginResult>()
                {
                    @Override
                    public void onSuccess(LoginResult loginResult)
                    {
                        GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback()
                        {

                            @Override
                            public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse)
                            {
                                try
                                {
                                    faceBookActionsListener.loginActions(jsonObject.getString(FaceBookSdkConst.NAME));
                                }
                                catch (JSONException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        }).executeAsync();

                    }

                    @Override
                    public void onCancel()
                    {
                    }

                    @Override
                    public void onError(FacebookException error)
                    {
                    }
                }
        );
    }

    public void logOut(FaceBookActionsListener faceBookActionsListener)
    {
        LoginManager.getInstance().logOut();
        AccountManager accountManager = (AccountManager) rootContext.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accounts = accountManager.getAccounts();
        for (int index = 0; index < accounts.length; index++)
        {
            if (accounts[index].type.equals(FaceBookSdkConst.FACEBOOK_TAG))
            {
                accountManager.removeAccount(accounts[index], null, null);
            }
        }
        faceBookActionsListener.logOutActions();
    }

    public void updateStatus(String message, final FaceBookActionsListener faceBookActionsListener)
    {
        Bundle parameters = new Bundle();
        parameters.putString(FaceBookSdkConst.MESSAGE, message);

        new GraphRequest(AccessToken.getCurrentAccessToken(),
                FaceBookSdkConst.FEED_PATH, parameters, HttpMethod.POST, new GraphRequest.Callback()
        {
            @Override
            public void onCompleted(GraphResponse graphResponse)
            {
                if (graphResponse.getError() == null)
                {
                    faceBookActionsListener.updateStatusActions(rootContext.getString(R.string.successfully));
                }
                else
                {
                    faceBookActionsListener.updateStatusActions(rootContext.getString(R.string.error));
                }
            }
        }
        ).executeAsync();
    }

    private String searchNearestPlaceId()
    {
        final String[] place = new String[1];

        Bundle parameters = new Bundle();
        parameters.putString(FaceBookSdkConst.TYPE, FaceBookSdkConst.PLACE);
        parameters.putString(FaceBookSdkConst.CENTER, LocationUtils.getStrLocation(rootContext));
        parameters.putString(FaceBookSdkConst.DISTANCE, String.valueOf(minDistance));
        parameters.putString(FaceBookSdkConst.LIMIT, FaceBookSdkConst.LIMIT_1);

        new GraphRequest(getAppAccessToken(), FaceBookSdkConst.SEARCH_PATH, parameters, HttpMethod.GET, new GraphRequest.Callback()
        {
            @Override
            public void onCompleted(GraphResponse graphResponse)
            {

                place[0] = new JSONUtils(graphResponse).getPlaceId();
            }
        }).executeAndWait();

        return place[0];
    }

    public AccessToken getAppAccessToken()
    {
        if (appAccessToken == null)
        {
            appAccessToken = GsonUtils.restoreToken(SettingsFacade_.getInstance_(rootContext).getToken());
        }

        if (appAccessToken == null || appAccessToken.isExpired())
        {

            Bundle parameters = new Bundle();
            parameters.putString(FaceBookSdkConst.CLIENT_ID, FaceBookSdkConst.CLIENT_ID_VALUE);
            parameters.putString(FaceBookSdkConst.CLIENT_SECRET, FaceBookSdkConst.CLIENT_SECRET_VALUE);
            parameters.putString(FaceBookSdkConst.GRANT_TYPE, FaceBookSdkConst.GRANT_TYPE_VALUE);

            new GraphRequest(AccessToken.getCurrentAccessToken(), FaceBookSdkConst.TOKEN_PATH, parameters, HttpMethod.GET,
                    new GraphRequest.Callback()
                    {
                        @Override
                        public void onCompleted(GraphResponse graphResponse)
                        {
                            try
                            {
                                appAccessToken = TokenUtils.createAppAccessToken
                                        (graphResponse.getJSONObject().getString(FaceBookSdkConst.ACCESS_TOKEN), rootContext);
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }

                    }
            ).executeAndWait();
        }

        return appAccessToken;
    }

    public String sharePicture(Bitmap image, final String message)
    {
        final String[] response = new String[1];

        Bundle parameters = new Bundle();
        String placeId = getNearestPlaceId();


        parameters.putString(FaceBookSdkConst.PLACE, placeId);

        GraphRequest.newUploadPhotoRequest(
                AccessToken.getCurrentAccessToken(),
                FaceBookSdkConst.PHOTOS_PATH, image, message, parameters, new GraphRequest.Callback()
                {
                    @Override
                    public void onCompleted(GraphResponse graphResponse)
                    {
                        if (graphResponse.getError() == null)
                        {
                            response[0] = rootContext.getString(R.string.uploaded_image);
                        }
                        else
                        {
                            response[0] = rootContext.getString(R.string.error_uploading_image);
                        }
                    }
                }
        ).executeAndWait();
        return response[0];
    }

    private String getNearestPlaceId()
    {
        String placeId = FaceBookSdkConst.ERROR;

        while (placeId.equals(FaceBookSdkConst.ERROR))
        {
            placeId = searchNearestPlaceId();
            increaseMaxDistance();
        }
        setMinDistance();
        return placeId;
    }

    public void getImageData(final FaceBookActionsListener faceBookActionsListener)
    {
        Bundle parameters = new Bundle();
        parameters.putString(FaceBookSdkConst.FIELDS, FaceBookSdkConst.IMAGE_FIELDS_VALUE);

        new GraphRequest(AccessToken.getCurrentAccessToken(), FaceBookSdkConst.PHOTOS_UPLOADED_PATH, parameters, HttpMethod.GET,
                new GraphRequest.Callback()
                {
                    @Override
                    public void onCompleted(GraphResponse graphResponse)
                    {
                        faceBookActionsListener.refreshActions(new JSONUtils(graphResponse).getListPhotos());
                    }
                }
        ).executeAsync();
    }

    private void increaseMaxDistance()
    {
        minDistance = minDistance * 3;
    }

    private void setMinDistance()
    {
        minDistance = MIN_DISTANCE;
    }
}
