package com.onregs.onregs_mapping.facebooksdk;

import java.util.Arrays;
import java.util.List;

/**
 * Created by vadim on 21.10.2015.
 */
public class FaceBookSdkConst
{


    public static final List<String> PERMISSIONS = Arrays.asList(
            "public_profile, publish_actions, user_posts, user_location, user_tagged_places");

    public static final String ID = "id";
    public static final String NAME = "name";


    public static final String FACEBOOK_TAG = "com.facebook.auth.login";

    public static final String TYPE = "type";
    public static final String LIMIT = "limit";
    public static final String FIELDS = "fields";

    public static final String MESSAGE = "message";
    public static final String CENTER = "center";
    public static final String DISTANCE = "distance";
    public static final String PLACE = "place";
    public static final String LOCATION = "location";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";
    public static final String IMAGES = "images";

    public static final String FEED_PATH = "me/feed";
    public static final String POSTS_PATH = "me/posts";
    public static final String SEARCH_PATH = "search";
    public static final String TOKEN_PATH = "oauth/access_token";
    public static final String PHOTOS_PATH = "me/photos";
    public static final String PHOTOS_UPLOADED_PATH = "me/photos/uploaded";


    public static final String DATA = "data";
    public static final String SOURCE = "source";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String HEIGHT = "height";
    public static final String WIDTH = "width";

    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_SECRET = "client_secret";
    public static final String GRANT_TYPE = "grant_type";

    public static final String CLIENT_ID_VALUE = "536919553150221";
    public static final String CLIENT_SECRET_VALUE = "cbe91c55d08cf5c5358768411617d175";
    public static final String GRANT_TYPE_VALUE = "client_credentials";
    public static final String IMAGE_FIELDS_VALUE = "place,name,images";
    public static final String LIMIT_1 = "1";

    public static final String ERROR = "-1";
}
