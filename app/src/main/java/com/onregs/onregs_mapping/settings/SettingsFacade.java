package com.onregs.onregs_mapping.settings;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

/**
 * Created by vadim on 19.10.2015.
 */

@EBean(scope = EBean.Scope.Singleton)
public class SettingsFacade
{
    @Pref
    protected Settings_ settings;

    public void setUser(String user)
    {
        settings.edit().user().put(user).apply();
    }

    public String getUser()
    {
        return settings.user().get();
    }

    public void setAppToken(String token)
    {
        settings.edit().appToken().put(token).apply();
    }

    public String getToken()
    {
        return settings.appToken().get();
    }
}
