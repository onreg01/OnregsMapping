package com.onregs.onregs_mapping.settings;

import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by vadim on 19.10.2015.
 */
@SharedPref(SharedPref.Scope.APPLICATION_DEFAULT)
public interface Settings
{
    @DefaultString("")
    String user();

    @DefaultString("")
    String appToken();
}
